# PixelPublicPlot-SyncErrors

A simple python macro which runs over ATLAS 2018 data runs to display synchronization errors for the different pixel layers.

To run the code:

 - you will need a folder named runs/ which contains the list of data runs.
 - you can run the code by simply typing:
 

   setupATLAS && lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"

   source doAllPlots.sh

   --> which internally calls   python plotPublic_MergeFive.py -o file_out.root -n 1 -p TOT

   
   * -o options sets the output file which will be saved also as a .pdf
   * -n is a boolean variable which sets if you want to merge runs in groups or if you prefer each run to be displayed (possible values 0/1)
   * -p the type of plot you want to do: TOT,MOD,ROD = {"total sync error","module sync error", "ROD sync error"}
   * the script will internally read a file called "DatesWithLumi.txt" in the format:
     run_number date luminosity

 - you need a folder which contains the ATLAS Style settings. 