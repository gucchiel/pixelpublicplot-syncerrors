import sys, getopt

import ROOT
from ROOT import TH2F, TFile, TCanvas, TH1F, gROOT, TMultiGraph, TDatime, TPad, TLine
gROOT.LoadMacro("./macros/AtlasStyle.C")
gROOT.LoadMacro("./macros/AtlasLabels.C")
gROOT.LoadMacro("./macros/AtlasUtils.C")
ROOT.SetAtlasStyle()

## modules
import os
import sys
import re
import subprocess
from array import array
import argparse
import os.path

ROOT.gStyle.SetPaintTextFormat("2.2f");
ROOT.gROOT.SetBatch(True)

import sys, getopt


def sameDayAsTomorrow(dates,index):

   if(index==(len(dates)-1)):
      return False
   else:
      if(dates[index].Convert()==dates[index+1].Convert()):
         return True
      else: return False


def returnMeHistogramName(layer,plot_type):

   histo_name = ''
   if(plot_type=="TOT"):
      histo_name = "SyncErrors_"+layer+"_byPostProcess"
   elif(plot_type=="MOD"):
      histo_name = "SyncErrors_Mod_"+layer
   elif(plot_type=="ROD"):
      histo_name = "SyncErrors_ROD_"+layer

   return histo_name   

def getSyncError(files_list, layer, dates, plot_type):

   syncError = []
   number_of_same_day_runs=1
   skimmed_dates = []
   index=0

   #here return plot to look for.
   histogram_name = returnMeHistogramName(layer,plot_type)

   mean = 0
   err  = 0
   bins = 0

   for histo in files_list:
      graph = ROOT.TH2F()
      graph = histo.Get(histogram_name)
      binsX = graph.GetNbinsX()
      binsY = graph.GetNbinsY()
      points = binsX*binsY

      normalization_factor = 1
      if(plot_type == "MOD" or plot_type=="ROD"):
         normalization_factor = histo.Get("Events_per_lumi").GetEntries()
         graph.Scale(1./normalization_factor)
         
      for x in range(binsX):
         for y in range(binsY):
            if(x==6 and y==7 and layer=="B0"):
               continue
            else:
               mean = mean + graph.GetBinContent(x,y)
            
      if(sameDayAsTomorrow(dates,index)==True):
         number_of_same_day_runs += 1
         index +=1
         continue

      else:
         mean = mean/(points*number_of_same_day_runs)
         syncError.append(mean)
         skimmed_dates.append(dates[index])
         index += 1
         mean =0
         number_of_same_day_runs=1
         
   return syncError,skimmed_dates   

def getLumi(lumi, dates):

   lumi_skimmed = []
   number_of_same_day_runs=1
   skimmed_dates = []
   index=0

   mean = 0

   for i in range(len(lumi)):
      mean += lumi[i]
      if(sameDayAsTomorrow(dates,index)==True):
         number_of_same_day_runs += 1
         index +=1
         continue

      else:
         mean = mean/(number_of_same_day_runs)
         lumi_skimmed.append(mean)
         index += 1
         mean =0
         number_of_same_day_runs=1
         
   return lumi_skimmed


def readDatesFromFile():

   dates = []
   is_run_or_not = []
   f = []
   lumi = []

   filepath = 'DatesWithLumi.txt'  
   file_n=0
   with open(filepath) as fp:  
      line = fp.readline()
      while line:
         file_n+=1
         line = fp.readline()
         if(len(line.strip()) < 1):
            break
         run = line[0:6]
         date = ROOT.TDatime(int(line[13:17]), int(line[10:12]),int(line[7:9]),0,0,0)
         lumi.append(float(line[18:-1])/1000)
         is_run_or_not.append(run)
         dates.append(date)
         if(os.path.isfile("/afs/cern.ch/user/g/gucchiel/PixelPublicPlots/runs/run"+run+".root")):
            f.append( ROOT.TFile("/afs/cern.ch/user/g/gucchiel/PixelPublicPlots/runs/run"+run+".root","READ"))
         
   return f,dates,is_run_or_not,lumi

def moreThanTwoDayDifference(dates, index):

   if(index==(len(dates)-1)):
      return False
   else:
      day1 = dates[index].GetDay()
      day2 = dates[index+1].GetDay()
      month1 = dates[index].GetMonth()
      month2 = dates[index+1].GetMonth()
      if(((month2-month1)>=0) and ((day2-day1)>4)):
         return True
      else: return False

def skimmedSet(tmp_syncErr,i_syncErr,divisor):

   tmp = []
   i_tmp = []
   
   for layer in range(5):
      tmp_inside = []
      i_tmp_inside = []
      for i in range(0,len(tmp_syncErr[layer]),+divisor):
         mean=0
         if((i+divisor-1)<(len(tmp_syncErr[layer]))):
            mean = sum(tmp_syncErr[layer][i:i+divisor])
            mean = mean/divisor
            tmp_inside.append(mean)
            i_tmp_inside.append(i_syncErr[layer][i+divisor-1])            
         else:
            mean = sum(tmp_syncErr[layer][i:i+len(tmp_syncErr[layer])])
            mean = mean/(len(tmp_syncErr[layer])-i)
            tmp_inside.append(mean)
            i_tmp_inside.append(i_syncErr[layer][len(tmp_syncErr[layer])-1])            

      tmp.append(tmp_inside)
      i_tmp.append(i_tmp_inside)     

   return tmp, i_tmp

def skimmedLumi(tmp_lumi,i_lumi,divisor):

   tmp = []
   i_tmp = []
   
   for i in range(0,len(tmp_lumi),+divisor):
      mean=0
      if((i+divisor-1)<len(tmp_lumi)):
         mean = sum(tmp_lumi[i:i+divisor])
         mean = mean/divisor
         tmp.append(mean)
         i_tmp.append(i_lumi[i+divisor-1])
      else:
         mean= sum(tmp_lumi[i:i+len(tmp_lumi)])
         mean= mean/(len(tmp_lumi)-i)
         tmp.append(mean)   
         i_tmp.append(i_lumi[len(tmp_lumi)-1])
      
   return tmp, i_tmp

def divideSetInto5(syncErr, skimmed_dates, the_start):

   tmp = []
   i_tmp = []

   for i in range(5):
      tmp_inside = []
      i_tmp_inside = []
      to_return = the_start
      sync = syncErr[i]
      for value in skimmed_dates[the_start:len(skimmed_dates)]:
         if(moreThanTwoDayDifference(skimmed_dates,to_return)==True):
            i_tmp_inside.append(to_return)
            tmp_inside.append(sync[to_return])
            to_return +=1
            break
         else:
            tmp_inside.append(sync[to_return])
            i_tmp_inside.append(to_return)
            to_return+=1

      tmp.append(tmp_inside)  
      i_tmp.append(i_tmp_inside)  
      
   return tmp, i_tmp, to_return

def divideLumiInto5(lumi, skimmed_dates, the_start):

   tmp = []
   i_tmp = []

   to_return = the_start
   for value in skimmed_dates[the_start:len(skimmed_dates)]:
      if(moreThanTwoDayDifference(skimmed_dates,to_return)==True):
         tmp.append(lumi[to_return])
         i_tmp.append(to_return)
         to_return +=1
         break
      else:
         tmp.append(lumi[to_return])
         i_tmp.append(to_return)
         to_return+=1
      
   return tmp, i_tmp, to_return

def main(argv):
   outputfile = ''
   nmerge = ''
   plot_type = ''
   try:
      opts, args = getopt.getopt(argv,"ho:n:p:",["ofile=","merge=","plot="])
   except getopt.GetoptError:
      print 'test.py -o <outputfile> -n <nmerge> -p <plot>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'test.py -o <outputfile> -n <nmerge> -p <plot>'
         sys.exit()
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-n", "--merge"):
         nmerge = arg
      elif opt in ("-p", "--plot"):
         plot_type = arg
   print 'Output file is "', outputfile
   print 'Runs to merge "', nmerge
   print 'You chose to plot "', plot_type #this can be either TOT,MOD or ROD

   merge=int(nmerge)
   if(merge==1):merge=True
   elif(merge==0):merge=False

   
   
   f = []

   dates = []
   is_existing = []
   lumi = []
   f, dates, is_existing,lumi = readDatesFromFile()

   canvas = ROOT.TCanvas("canv","canv",1400,600)
   canvas.SetLeftMargin(0.15)
   canvas.SetRightMargin(0.20)

   pad1 = ROOT.TPad("pad","",0,0,1,1);
   pad1.SetLeftMargin(0.1)
   pad1.SetRightMargin(0.1)
   pad1.Draw();
   pad1.cd();

   #layers = ["IBL2D","B0","B1","B2","ECA","ECC"]
   layers = ["B0","B1","B2","ECA","ECC"]
   #layer_legend = ["IBL","BLAYER","LAYER1","LAYER2","ENDCAP-A","ENDCAP-C"]
   layer_legend = ["B-LAYER","LAYER1","LAYER2","ENDCAP-A","ENDCAP-C"]

   #Create template histogram for overlaying graphs

   backgroundHisto = ROOT.TMultiGraph()

   syncErr = []
   syncErr0 , syncErr1, syncErr2, syncErr3, syncErr4 = [],[],[],[],[]
   i_syncErr0 , i_syncErr1, i_syncErr2, i_syncErr3, i_syncErr4 = [],[],[],[],[]

   skimmed_dates = []

   lumi_value = []
   lumi0 , lumi1, lumi2, lumi3, lumi4 = [],[],[],[],[]
   i_lumi0 , i_lumi1, i_lumi2, i_lumi3, i_lumi4 = [],[],[],[],[]

   for layer in range(5):      
      tmp_array = []
      tmp_array , skimmed_dates = getSyncError(f,layers[layer],dates,plot_type)
      syncErr.append(tmp_array)
      
   lumi_value = getLumi(lumi,dates)

   keep_track=0   
   for group in range(5):
      if(group==0): 
         syncErr0, i_syncErr0, keep_track = divideSetInto5(syncErr, skimmed_dates, keep_track)
      elif(group==1): 
         syncErr1, i_syncErr1, keep_track = divideSetInto5(syncErr, skimmed_dates, keep_track)
      elif(group==2): 
         syncErr2, i_syncErr2, keep_track = divideSetInto5(syncErr, skimmed_dates, keep_track)
      elif(group==3): 
         syncErr3, i_syncErr3, keep_track = divideSetInto5(syncErr, skimmed_dates, keep_track)
      elif(group==4): 
         syncErr4, i_syncErr4, keep_track = divideSetInto5(syncErr, skimmed_dates, keep_track)
   to_start=0   

   #print len(syncErr1[0])
   #print len(syncErr2[0])
   #print len(syncErr3[0])
   #print len(syncErr4[0])

   keep_track=0   
   for group in range(5):
      if(group==0): 
         lumi0, i_lumi0, keep_track = divideLumiInto5(lumi_value, skimmed_dates, keep_track)
      elif(group==1): 
         lumi1, i_lumi1, keep_track = divideLumiInto5(lumi_value, skimmed_dates, keep_track)
      elif(group==2): 
         lumi2, i_lumi2, keep_track = divideLumiInto5(lumi_value, skimmed_dates, keep_track)
      elif(group==3): 
         lumi3, i_lumi3, keep_track = divideLumiInto5(lumi_value, skimmed_dates, keep_track)
      elif(group==4): 
         lumi4, i_lumi4, keep_track = divideLumiInto5(lumi_value, skimmed_dates, keep_track)
   to_start=0   

   if(merge==True):
      syncErr0,i_syncErr0 = skimmedSet(syncErr0,i_syncErr0,4)
      syncErr1,i_syncErr1 = skimmedSet(syncErr1,i_syncErr1,6)
      syncErr2,i_syncErr2 = skimmedSet(syncErr2,i_syncErr2,5)
      syncErr3,i_syncErr3 = skimmedSet(syncErr3,i_syncErr3,2)
      syncErr4,i_syncErr4 = skimmedSet(syncErr4,i_syncErr4,2)
      lumi0,i_lumi0 = skimmedLumi(lumi0,i_lumi0,4)
      lumi1,i_lumi1 = skimmedLumi(lumi1,i_lumi1,6)
      lumi2,i_lumi2 = skimmedLumi(lumi2,i_lumi2,5)
      lumi3,i_lumi3 = skimmedLumi(lumi3,i_lumi3,2)
      lumi4,i_lumi4 = skimmedLumi(lumi4,i_lumi4,2)

   #print "Printing sizes"
   #print len(syncErr0[0])
   #print len(syncErr1[0])
   #print len(syncErr2[0])
   #print len(syncErr3[0])
   #print len(syncErr4[0])

   syncErrorPlot0,syncErrorPlot1,syncErrorPlot2,syncErrorPlot3,syncErrorPlot4 =[],[],[],[],[]

   leg = ROOT.TLegend(0.50,0.80,0.865,0.90)

   lumiHisto = ROOT.TMultiGraph()
   color = [2,4,6,3,7]
   style = [21,22,23,24,25]

   for layer in range(5):      
      index=0
      graph=ROOT.TGraph()
      syncErrorPlot0.append(graph)
      for a in syncErr0[layer]:
         syncErrorPlot0[layer].SetPoint(index,skimmed_dates[i_syncErr0[layer][index]].Convert(), syncErr0[layer][index])
         syncErrorPlot0[layer].SetMarkerSize(1.1)   
         syncErrorPlot0[layer].SetMarkerStyle(style[layer])
         syncErrorPlot0[layer].SetMarkerColor(color[layer])      
         syncErrorPlot0[layer].SetLineColor(color[layer])      
         syncErrorPlot0[layer].SetLineWidth(1)      
         index = index+1
      
      backgroundHisto.Add(syncErrorPlot0[layer],"AI PL")
      leg.AddEntry(syncErrorPlot0[layer],layer_legend[layer],"lep")         

   for layer in range(5):      
      index=0
      graph=ROOT.TGraph()
      syncErrorPlot1.append(graph)
      for a in syncErr1[layer]:
         syncErrorPlot1[layer].SetPoint(index,skimmed_dates[i_syncErr1[layer][index]].Convert(), syncErr1[layer][index])
         syncErrorPlot1[layer].SetMarkerSize(1.1)   
         syncErrorPlot1[layer].SetMarkerStyle(style[layer])
         syncErrorPlot1[layer].SetMarkerColor(color[layer])      
         syncErrorPlot1[layer].SetLineColor(color[layer])      
         syncErrorPlot1[layer].SetLineWidth(1)      
         index = index+1

      backgroundHisto.Add(syncErrorPlot1[layer],"AI PL")

   for layer in range(5):      
      index=0
      graph=ROOT.TGraph()
      syncErrorPlot2.append(graph)
      for a in syncErr2[layer]:
         syncErrorPlot2[layer].SetPoint(index,skimmed_dates[i_syncErr2[layer][index]].Convert(), syncErr2[layer][index])
         syncErrorPlot2[layer].SetMarkerSize(1.1)   
         syncErrorPlot2[layer].SetMarkerStyle(style[layer])
         syncErrorPlot2[layer].SetMarkerColor(color[layer])      
         syncErrorPlot2[layer].SetLineColor(color[layer])      
         syncErrorPlot2[layer].SetLineWidth(1)      
         index = index+1

      backgroundHisto.Add(syncErrorPlot2[layer], "AI PL")

   for layer in range(5):      
      index=0
      graph=ROOT.TGraph()
      syncErrorPlot3.append(graph)
      for a in syncErr3[layer]:
         syncErrorPlot3[layer].SetPoint(index,skimmed_dates[i_syncErr3[layer][index]].Convert(), syncErr3[layer][index])
         syncErrorPlot3[layer].SetMarkerSize(1.1)   
         syncErrorPlot3[layer].SetMarkerStyle(style[layer])
         syncErrorPlot3[layer].SetMarkerColor(color[layer])      
         syncErrorPlot3[layer].SetLineColor(color[layer])      
         syncErrorPlot3[layer].SetLineWidth(1)      
         index = index+1

      backgroundHisto.Add(syncErrorPlot3[layer], "AI PL")   

   for layer in range(5):      
      index=0
      graph=ROOT.TGraph()
      syncErrorPlot4.append(graph)
      for a in syncErr4[layer]:
         syncErrorPlot4[layer].SetPoint(index,skimmed_dates[i_syncErr4[layer][index]].Convert(), syncErr4[layer][index])
         syncErrorPlot4[layer].SetMarkerSize(1.1)   
         syncErrorPlot4[layer].SetMarkerStyle(style[layer])
         syncErrorPlot4[layer].SetMarkerColor(color[layer])      
         syncErrorPlot4[layer].SetLineColor(color[layer])      
         syncErrorPlot4[layer].SetLineWidth(1)      
         index = index+1

      backgroundHisto.Add(syncErrorPlot4[layer], "AI PL")   
      
   backgroundHisto.GetYaxis().SetTitle("Average fraction of module with sync. errors")
   backgroundHisto.GetXaxis().SetTitle("Date [dd/mm]")
   backgroundHisto.GetXaxis().Set
   
   if(plot_type=="TOT"):
      backgroundHisto.GetYaxis().SetRangeUser(0.000004,1.4)         
   elif(plot_type=="ROD"):
      backgroundHisto.GetYaxis().SetRangeUser(0.000004,1.4)         
   elif(plot_type=="MOD"):
      backgroundHisto.GetYaxis().SetRangeUser(0.000004,1.4)         
   
   backgroundHisto.GetYaxis().SetTitleOffset(0.8)
   backgroundHisto.GetXaxis().SetTitleOffset(1)
   backgroundHisto.GetXaxis().SetTimeDisplay(1)
   backgroundHisto.GetXaxis().SetTimeFormat("%d/%m")
   backgroundHisto.GetXaxis().SetLimits(TDatime(2018,04,15,0,0,0).Convert(),TDatime(2018,11,01,0,0,0).Convert())
   backgroundHisto.Draw("AI")

   pad1.SetTicks(0,0)   

   lumiPlot0=ROOT.TGraph()

   for a in range(len(lumi0)):
      lumiPlot0.SetPoint(a,skimmed_dates[i_lumi0[a]].Convert(), lumi0[a])
      lumiPlot0.GetXaxis().SetTimeDisplay(1)
      lumiPlot0.GetXaxis().SetTimeFormat("%d/%m")
      lumiPlot0.SetMarkerSize(1.1)   
      lumiPlot0.SetMarkerStyle(20)
      lumiPlot0.SetMarkerColor(14)      
      lumiPlot0.SetLineColor(14)      
      lumiPlot0.SetLineWidth(1)      

   lumiPlot1=ROOT.TGraph()
   for a in range(len(lumi1)):
      lumiPlot1.SetPoint(a,skimmed_dates[i_lumi1[a]].Convert(), lumi1[a])
      lumiPlot1.GetXaxis().SetTimeDisplay(1)
      lumiPlot1.GetXaxis().SetTimeFormat("%d/%m")
      lumiPlot1.SetMarkerSize(1.1)   
      lumiPlot1.SetMarkerStyle(20)
      lumiPlot1.SetMarkerColor(14)      
      lumiPlot1.SetLineColor(14)      
      lumiPlot1.SetLineWidth(1)      
      
   lumiPlot2=ROOT.TGraph()
   for a in range(len(lumi2)):
      lumiPlot2.SetPoint(a,skimmed_dates[i_lumi2[a]].Convert(), lumi2[a])
      lumiPlot2.GetXaxis().SetTimeDisplay(1)
      lumiPlot2.GetXaxis().SetTimeFormat("%d/%m")
      lumiPlot2.SetMarkerSize(1.1)   
      lumiPlot2.SetMarkerStyle(20)
      lumiPlot2.SetMarkerColor(14)      
      lumiPlot2.SetLineColor(14)      
      lumiPlot2.SetLineWidth(1)      
      
   lumiPlot3=ROOT.TGraph()
   for a in range(len(lumi3)):
      lumiPlot3.SetPoint(a,skimmed_dates[i_lumi3[a]].Convert(), lumi3[a])
      lumiPlot3.GetXaxis().SetTimeDisplay(1)
      lumiPlot3.GetXaxis().SetTimeFormat("%d/%m")
      lumiPlot3.SetMarkerSize(1.1)   
      lumiPlot3.SetMarkerStyle(20)
      lumiPlot3.SetMarkerColor(14)
      lumiPlot3.SetLineColor(14)
      lumiPlot3.SetLineWidth(1)      

   lumiPlot4=ROOT.TGraph()
   for a in range(len(lumi4)):
      lumiPlot4.SetPoint(a,skimmed_dates[i_lumi4[a]].Convert(), lumi4[a])
      lumiPlot4.GetXaxis().SetTimeDisplay(1)
      lumiPlot4.GetXaxis().SetTimeFormat("%d/%m")
      lumiPlot4.SetMarkerSize(1.1)   
      lumiPlot4.SetMarkerStyle(20)
      lumiPlot4.SetMarkerColor(14)
      lumiPlot4.SetLineColor(14)      
      lumiPlot4.SetLineWidth(1)      
      
   leg.AddEntry(lumiPlot4,"Luminosity","lep")   
   lumiHisto.Add(lumiPlot0,"PLY+")
   lumiHisto.Add(lumiPlot1,"PLY+")
   lumiHisto.Add(lumiPlot2,"PLY+")
   lumiHisto.Add(lumiPlot3,"PLY+")
   lumiHisto.Add(lumiPlot4,"PLY+")
   
   #canvas.SetLogy()
   pad1.SetLogy()

   leg.SetNColumns(2)
   leg.SetBorderSize(0)
   leg.SetFillColor(0)
   leg.SetFillStyle(0)
   leg.SetTextSize(0.03)
   leg.SetTextFont(42)

   label=""
   print plot_type
   if(plot_type == "MOD"):
      label = ", MOD"
   elif(plot_type== "ROD"):
      label = ", ROD"

   leg.Draw()
   xLabelPos = 0.150
   rsplit = 0.43
   ROOT.ATLASLabel(xLabelPos,0.875, "Pixel Internal",1)
   ROOT.myText(xLabelPos,0.82,1       ,"#sqrt{s}=13 TeV"+label,0.,0.13*rsplit)
   ROOT.myText(xLabelPos,0.76,1       ,"2018 data",0.,0.13*rsplit)
   latex = ROOT.TLatex()
   latex.SetTextAlign(12)
   latex.SetTextSize(0.13*rsplit)
   latex.SetNDC()
   latex.SetTextFont(72)
   latex.SetTextColor(1)

   #Nobeam lines
   max_y_line=0.1
   if(plot_type=="ROD"):
      max_y_line=0.1
   elif(plot_type=="MOD"):
      max_y_line=0.1

   nobeam1_b = ROOT.TLine(TDatime(2018,06,12,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,06,12,0,0,0).Convert(),max_y_line)
   nobeam1_e = ROOT.TLine(TDatime(2018,06,25,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,06,25,0,0,0).Convert(),max_y_line)
   nobeam2_b = ROOT.TLine(TDatime(2018,07,23,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,07,23,0,0,0).Convert(),max_y_line)
   nobeam2_e = ROOT.TLine(TDatime(2018,07,30,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,07,30,0,0,0).Convert(),max_y_line)
   nobeam3_b = ROOT.TLine(TDatime(2018,9,11,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,9,11,0,0,0).Convert(),max_y_line)
   nobeam3_e = ROOT.TLine(TDatime(2018,9,22,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,9,22,0,0,0).Convert(),max_y_line)
   nobeam4_b = ROOT.TLine(TDatime(2018,10,11,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,10,11,0,0,0).Convert(),max_y_line)
   nobeam4_e = ROOT.TLine(TDatime(2018,10,15,0,0,0).Convert(),ROOT.gPad.GetUymin(),TDatime(2018,10,15,23,59,0).Convert(),max_y_line)

   nobeam1_b.SetLineStyle(2), nobeam1_b.SetLineWidth(2)
   nobeam1_e.SetLineStyle(2), nobeam1_e.SetLineWidth(2)
   nobeam1_b.Draw(),nobeam1_e.Draw()
   nobeam2_b.SetLineStyle(2), nobeam2_b.SetLineWidth(2)
   nobeam2_e.SetLineStyle(2), nobeam2_e.SetLineWidth(2)
   nobeam2_b.Draw(),nobeam2_e.Draw()
   nobeam3_b.SetLineStyle(2), nobeam3_b.SetLineWidth(2)
   nobeam3_e.SetLineStyle(2), nobeam3_e.SetLineWidth(2)
   nobeam3_b.Draw(),nobeam3_e.Draw()
   nobeam4_b.SetLineStyle(2), nobeam4_b.SetLineWidth(2)
   nobeam4_e.SetLineStyle(2), nobeam4_e.SetLineWidth(2)
   nobeam4_b.Draw(),nobeam4_e.Draw()


   ROOT.myText(ROOT.gPad.GetUxmin()+0.357,ROOT.gPad.GetUymin()+0.4,1 ,"No beam",90,0.03)
   ROOT.myText(ROOT.gPad.GetUxmin()+0.513,ROOT.gPad.GetUymin()+0.4,1 ,"No beam",90,0.03)
   ROOT.myText(ROOT.gPad.GetUxmin()+0.720,ROOT.gPad.GetUymin()+0.4,1 ,"No beam",90,0.03)
   ROOT.myText(ROOT.gPad.GetUxmin()+0.830,ROOT.gPad.GetUymin()+0.4,1 ,"No beam",90,0.03)


   canvas.cd()
   
   pad2 = ROOT.TPad("overlay","",0,0,1,1);
   pad2.SetFillStyle(4000);
   pad2.SetFillColor(0);
   pad2.SetFrameFillStyle(4000);
   pad2.SetLeftMargin(0.1)
   pad2.SetRightMargin(0.1)
   pad2.Draw()
   pad2.cd()
   

   lumiHisto.GetYaxis().SetRangeUser(0.00000002,0.7)
   #lumiHisto.GetYaxis().SetTitleOffset(0.8)
   lumiHisto.GetXaxis().SetLabelOffset(99);
   lumiHisto.GetYaxis().SetLabelOffset(99);
   lumiHisto.GetXaxis().SetTimeDisplay(1)
   lumiHisto.GetXaxis().SetTimeFormat("%d/%m")
   lumiHisto.GetXaxis().SetLimits(TDatime(2018,04,15,0,0,0).Convert(),TDatime(2018,11,01,0,0,0).Convert())
   #lumiHisto.GetYaxis().SetRangeUser(ROOT.gPad.GetUymin()*1.8,ROOT.gPad.GetUxmax()*1.8)
   #pad2.SetTicks(0,0)


   lumiHisto.Draw("ALPY+")
   lumiHisto.Draw("ALPY+")

   ROOT.gPad.Update()
   lumiHisto.GetYaxis().SetNdivisions(0)

   axis = ROOT.TGaxis(ROOT.gPad.GetUxmax(),ROOT.gPad.GetUymin(),ROOT.gPad.GetUxmax(),ROOT.gPad.GetUymax(),0.00000002,0.7,510,"+L")
   axis.SetTitle("Average integrated luminosity [fb^{-1}/day]")
   axis.SetTitleOffset(0.8)
   axis.SetTitleFont(42)
   axis.SetTitleSize(0.045)
   axis.SetLabelFont(42)
   axis.SetLabelSize(0.05)
   axis.SetLabelOffset(0.005)
   axis.SetLabelColor(14)
   axis.SetLineColor(14)
   axis.Draw("same")
   #pad2.SetLogy(1)  
   #ROOT.gPad.SetLogy(1)

   canvas.Modified()
   canvas.Update()      
   canvas.Print(outputfile)
   canvas.SaveAs("plot_"+plot_type+".pdf")


if __name__ == "__main__":
   main(sys.argv[1:])
